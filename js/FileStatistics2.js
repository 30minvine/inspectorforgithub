
let fileName = localStorage.getItem('fileName')
let filePath = localStorage.getItem('filePath')
let owner = localStorage.getItem('owner')
let repo = localStorage.getItem('repo');
let barData,pieData;
let highestTotal = 0;
let listOfMonths = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct", "Nov", "Dec"]
// Adding Repository name and owner dynamically
document.getElementById("ownerAndRepo").innerHTML = `${owner} / ${repo}`
// Adding file name dynamically
document.getElementById("pathTitle").innerHTML = fileName
// Finish DOM Update

// refer the user back to the repository page when they have accidentally entered into the fileStatistic Page
if (localStorage.getItem("fileName") === null || localStorage.getItem("filePath") === null){
  window.location.href = "Repository.html";
}
// const contributorData = JSON.parse(localStorage.getItem('fileStats')).slice(0, 5)
//fileName = localStorage.getItem('fileName'),
//filePath = localStorage.getItem('filePath'),
//owner = localStorage.getItem('owner'),
//repo = localStorage.getItem('repo');

// Extracting contributor data from file path
const fetchBlame = async () => {
  let token = "42662a45e8b4a04d3fc4e4164d39606dff4f7736"
  const api_call = await fetch('https://api.github.com/graphql', {
      method: 'POST',
    headers: {
        'Authorization': `bearer ${token}`
    },
    body: JSON.stringify({
      query: `query {
          repository(owner:\"${owner}\", name:\"${repo}\") {
          defaultBranchRef {
              target {
              ... on Commit {
                  blame(path: \"${filePath}\") {
                  ranges {
                      startingLine
                    endingLine
                    age
                    commit {
                      author {
                          user {
                          login
                        }
                      }
                    }
                    commit{
                      author{
                        user{
                          avatarUrl
                        }
                      }
                    }

                  }
                }
            }
            }
        }
        }
    }`
})
  })
  const data = await api_call.json();
  return {data: data, name: "fetchBlame"}
}

const fetchDateAndChanges = async () => {
  let token = "42662a45e8b4a04d3fc4e4164d39606dff4f7736"
  let yearAgoDate = retrieveOneYearAgoDate()
  const api_call = await fetch('https://api.github.com/graphql', {
      method: 'POST',
    headers: {
        'Authorization': `bearer ${token}`
    },
    body: JSON.stringify({
      query: `query {
        repository(owner: "${owner}", name: "${repo}") {
          object(expression: "master") {
            ... on Commit {
              history(first: 50, path: "${filePath}") {
                edges {
                  node {
                    additions
                    deletions
                    committedDate
                    author {
                      user {
                        login
                      }
                    }
                  }
                }
              }
            }
          }
        }
    }`
})
  })
  const data = await api_call.json();
  return {data: data, name: "fetchDateAndChanges"}
}

// This function returns a date object of today's date - 1 year
// September 29th June 2019 => September 29th June 2018
function retrieveOneYearAgoDate() {
  let today = new Date()
  // Subtracting 1 year from today's date
  today.setFullYear(today.getFullYear()-1)
  today.setDate(1)
  return today
}

const showData = async (callback,MODE) => {
  let res = await callback()
  console.log("showData() called");
    if (res.name === "fetchBlame")
    {
      let blameRange = res.data.data.repository.defaultBranchRef.target.blame.ranges
      produceBlameTally(blameRange)
    }
    else if (res.name === "fetchDateAndChanges")
    {
      let statsRange = res.data.data.repository.object.history.edges
      produceStatsAndDateTally(statsRange,MODE)
    }

}


const syncedDisplay = async (id,MODE) => {
  // document.getElementById("pie").innerHTML = ""
  // document.getElementById("bar").innerHTML = ""
  //await showData(fetchDateAndChanges,MODE)
  //await showData(fetchBlame)
  //changeToggleButton();
  console.log("syncedDisplay() called");
  await Promise.all([showData(fetchDateAndChanges,MODE),showData(fetchBlame)])
  displayData(MODE)
  return id;
}

function changeToggleButton(id, MODE) {
  id.innerHTML = '<div class="mdl-spinner mdl-js-spinner is-active" style="display: block"></div>';
  componentHandler.upgradeDom();
  syncedDisplay(id,MODE).then((id) => {
    if (MODE == "PERCENT") {
      id.setAttribute("onclick",'changeToggleButton(this,"ABSOLUTE")');
      id.innerHTML = "ABSOLUTE";
    } else {
      id.setAttribute("onclick",'changeToggleButton(this,"PERCENT")');
      id.innerHTML = "PERCENT";
    }
  })
}

const produceStatsAndDateTally = (statsRange,MODE) => {
    let tallyOfContributors = produceTallyOfContributors(statsRange)
    let tallyOfMonths = {}
    try {
      for (let i=0;i<statsRange.length;i++)
      {
        let author = statsRange[i].node.author.user.login
        let changes = statsRange[i].node.additions + statsRange[i].node.deletions
        let commitDate = new Date(statsRange[i].node.committedDate)
        let month = listOfMonths[commitDate.getMonth()]
        let year = commitDate.getFullYear()

        if (tallyOfMonths[month+"-"+year]==undefined) {
          let newMonth = Object.assign({},tallyOfContributors)
          newMonth.month = month
          tallyOfMonths[month+"-"+year] = newMonth
        }
        tallyOfMonths[month+"-"+year][author] += changes // Points to the correct month
        tallyOfMonths[month+"-"+year].total += changes
      }
    } catch {}
    convertTallyIntoPercent(tallyOfMonths,MODE)
    barData = convertToCompatibleArray(tallyOfContributors,tallyOfMonths)

}

const convertToCompatibleArray = (contributors,monthTally) => {
  let compatibleData = [["Month"]]
  for (let author in contributors) {
    if (author=="total"||author=="month") {continue}
    compatibleData[0].push(author)
  }
  for (let key in monthTally) {
    let month = monthTally[key].month
    let monthSubTally = [key]
    for (let author in monthTally[key]) {
      if (author=="total"||author=="month") {continue}
      monthSubTally.push(monthTally[key][author])
    }
    compatibleData.push(monthSubTally)
  }
  return compatibleData
}

const convertTallyIntoPercent = (tallyOfMonths,MODE) => {
  for (let key in tallyOfMonths) {
    let total = tallyOfMonths[key].total
    if (total>highestTotal) {highestTotal=total}
    if (MODE==="PERCENT") {
      for (let author in tallyOfMonths[key]) {
        if (total==0) {break}
        if (author=="month") {continue}
        let changes = tallyOfMonths[key][author]
        tallyOfMonths[key][author] = Math.round((changes/total)*100)
      }
    }
  }
}

const produceTallyOfContributors = (statsRange) => {
  listOfContributors = {}
  try {
    for (let i=0;i<statsRange.length;i++)
    {
      let author = statsRange[i].node.author.user.login
      if (listOfContributors[author]==undefined)
      {
        listOfContributors[author] = 0
      }
    }
  } catch {}
  listOfContributors.total = 0
  return listOfContributors
}

const produceTallyOfMonths = (filler) => {
  let date = retrieveOneYearAgoDate()
  let tallyOfMonths = {}
  for (let i=0;i<13;i++)
  {
    let month = listOfMonths[date.getMonth()]
    let year = date.getFullYear()
    // Cloning the filler material to prevent cross references
    // and assigning the month to a property for quick access later
    b = Object.assign({},filler)
    b["month"] = month
    //
    tallyOfMonths[month+"-"+year] = b
    date.setMonth(date.getMonth()+1)  // Increment Month
  }
  return tallyOfMonths
}

const produceBlameTally = (blameRange) => {
    let tally = {}
    // As of now. Tally represents the number of commits made to a particular file
  for (let i=0;i<blameRange.length;i++)
  {
      let user = blameRange[i].commit.author.user
    let linesChanged = blameRange[i].endingLine-blameRange[i].startingLine+1
    if (user!==null)
    {
      if (tally[user.login]===undefined)
      {
          tally[user.login] = {linesChanged:linesChanged, avatarUrl: user.avatarUrl}
      } else
      {
          tally[user.login.linesChanged] += linesChanged
        }
    }
}
  let sortedTally = sortTallyIntoArray(tally, blameRange)
  pieData = sortedTally
  localStorage.setItem('fileStats', JSON.stringify(sortedTally))
}

const sortTallyIntoArray = (unsortedTally, blameRange) => {
    let unsortedArray = []
    let index = 0
    for (let name in unsortedTally)
    {
        //let avatar_url = blameRange[index].commit.author.user.avatarUrl
        //let nameScoreInstance = {name: name,score: unsortedTally[name],avatarUrl: avatar_url}
        let nameScoreInstance = {name: name,score: unsortedTally[name].linesChanged, avatarUrl: unsortedTally[name].avatarUrl}
        unsortedArray.push(nameScoreInstance)
        index += 1
    }
    // Sorting unsortedArray that contains names and scores
  unsortedArray.sort((a,b)=>{
      if (a.score > b.score) {
          return -1;
        } else if (b.score > a.score) {
            return 1;
    } else {
        return 0;
    }
})
  return unsortedArray;
}

function outputOfSortedTally(tally) {
    // For the tally input check end of produceBlameTally
    console.log(tally)
}

function displayData(MODE) {
    // Pie chart of top five contributors
    const contributorData = pieData
    let vAxisMax,vAxisTitle;
    if (MODE==="PERCENT") {
      vAxisMax = 100
      vAxisTitle = "% Contribution"
    } else if (MODE==="ABSOLUTE") {
      vAxisMax = highestTotal
      vAxisTitle = "Changes"
    }
    google.charts.load('current', { packages: ['corechart'] });
    google.charts.setOnLoadCallback(drawPie);
    google.charts.setOnLoadCallback(drawBar);

    listOfContributors();
    function drawPie() {
        // Define the chart to be drawn.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Contributor');
        data.addColumn('number', 'Lines');
        data.addRows(
            contributorData.map(e => [e.name, e.score])
        );
        var options = {
            //legend: { position: 'top', maxLines: 3 },
            title: 'Contributors:'
        };
        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById("pie"));

        // DELETE SPINNERS
        let spinners = document.getElementsByClassName("mdl-spinner mdl-js-spinner is-active");
        for (let i = 0; i < 2; i++) {
          spinners[i].style.display = "none";
        }

        const buttons = document.querySelectorAll('.mdl-button');
        buttons.forEach(element => {
          element.removeAttribute("style");
        });

        chart.draw(data, options);
    }

    function drawBar() {
        // Define the chart to be drawn.
        var data = new google.visualization.arrayToDataTable(barData);
        var options = {
            // legend: { position: 'top', maxLines: 3 },
            bar: {groupWidth : "95%"},
            isStacked: true,
            vAxis: {
              title: vAxisTitle,
              viewWindow: {
                min: 0,
                max: vAxisMax
              }
            },
            hAxis: {
              direction: -1
            },
            explorer: {
              axis: "horizontal",
              keepInBounds: true
            }
        };
        // Instantiate and draw the chart.
        var chart = new google.visualization.ColumnChart(document.getElementById("bar"));
        chart.draw(data, options);
    }

    function buildTableHead(){
        /*
        This function, similar to the Contributors Table, is used to build the Header for the table with columns {icon, filename, filepath, and size}
        */
        let tableHead  = document.createElement("thead");
        let newRow = document.createElement("tr");
        // create Header for contributor avatar
        let newHeader = document.createElement("th");
        newHeader.class += "mdl-layout--large-screen-only";
        newRow.appendChild(newHeader);

        // create Header for File Name
        newHeader = document.createElement("th");
        newHeader.class += "mdl-data-table__cell--non-numeric";
        newHeader.innerHTML = "Contributor";
        newRow.appendChild(newHeader);

        // create Header for File Size
        newHeader = document.createElement("th");
        newHeader.class += "mdl-data-table__cell--non-numeric";
        newHeader.innerHTML = "No. of Commits";
        newRow.appendChild(newHeader);

        tableHead.appendChild(newRow);
        return tableHead;
    }

    function buildTableBody(fileStats){
        /*
        This function is used to build the table body
        */

        // creating the body of the table
        let contributorData = JSON.parse(localStorage.getItem("fileStats"))
        let tableBody = document.createElement("tbody");
        tableBody.id = "contributorTBody";

        // function to be called when row is clicked
        var clickHandler = function(user) {
            return function() {
                localStorage.setItem('profile',user);
                window.location.href = "ContributorProfile.html";
            };
        };

        // looping through the fileList
        for (let index = 0; index < contributorData.length; index++){
            if (contributorData[index].name != "undefined"){
              let newRow = document.createElement("tr");

              let avatar = document.createElement("td");
              avatar.className += "mdl-data-table__cell--non-numeric";
              let avatar_pic = document.createElement('img')
              avatar_pic.src = contributorData[index].avatarUrl;
              avatar_pic.alt = contributorData[index].avatarUrl+"'s avatar";
              avatar_pic.width = 50;
              avatar_pic.height = 50;
              avatar.appendChild(avatar_pic);
              newRow.appendChild(avatar);

              // add filename to row
              let contributorName = document.createElement("td");
              contributorName.className += "mdl-data-table__cell--non-numeric";
              contributorName.innerHTML = contributorData[index].name
              newRow.appendChild(contributorName);

              // add size of the file to row
              let noOfCommits = document.createElement("td");
              noOfCommits.className += "mdl-data-table__cell--non-numeric";
              noOfCommits.innerHTML = contributorData[index].score
              newRow.appendChild(noOfCommits);

              newRow.onclick = clickHandler(contributorData[index].name);

              // append row to table
              tableBody.appendChild(newRow);

            }

        }

        return tableBody;
    }

    function listOfContributors(){
        let fileStats = pieData
        let table = document.getElementById("file_table");
        table.style.display = "table";
        //var fileStats = localStorage.getItem("fileStats")
        table.appendChild(buildTableHead());
        table.appendChild(buildTableBody(fileStats));

    }

};

syncedDisplay("ABSOLUTE")

    google.charts.load('current', { packages: ['corechart'] });
  //  google.charts.setOnLoadCallback(drawPie);
  //  google.charts.setOnLoadCallback(drawBar);



function buildTableHead(){
    /*
    This function, similar to the Contributors Table, is used to build the Header for the table with columns {icon, filename, filepath, and size}
    */
    let tableHead  = document.createElement("thead");
    let newRow = document.createElement("tr");
    // create Header for contributor avatar
    let newHeader = document.createElement("th");
    /*
    newHeader.class += "mdl-layout--large-screen-only";
    newRow.appendChild(newHeader);
    */

    // create Header for File Name
    newHeader = document.createElement("th");
    newHeader.class += "mdl-data-table__cell--non-numeric";
    newHeader.innerHTML = "Contributor";
    newRow.appendChild(newHeader);

    // create Header for File Size
    newHeader = document.createElement("th");
    newHeader.class += "mdl-data-table__cell--non-numeric";
    newHeader.innerHTML = "No. of Commits";
    newRow.appendChild(newHeader);

    tableHead.appendChild(newRow);
    return tableHead;
}

function buildTableBody(fileStats){
    /*
    This function is used to build the table body
    */

    // creating the body of the table
    let tableBody = document.createElement("tbody");
    tableBody.id = "contributorTBody";

    var clickHandler = function(user) {
        return function() {
            localStorage.setItem('profile',user);
            window.location.href = "ContributorProfile.html";
        };
    };


    // looping through the fileList
    for (let index = 0; index < contributorData.length; index++){
        let newRow = document.createElement("tr");
        /*
        let fileIcon = document.createElement("td");
        fileIcon.innerHTML = "<i class=\"material-icons transparent-file-button\">description</i>"
        newRow.appendChild(fileIcon);
        */

        // add filename to row
        let contributorName = document.createElement("td");
        contributorName.className += "mdl-data-table__cell--non-numeric";
        contributorName.innerHTML = contributorData[index].name
        newRow.appendChild(contributorName);

        // add size of the file to row
        let noOfCommits = document.createElement("td");
        noOfCommits.className += "mdl-data-table__cell--non-numeric";
        noOfCommits.innerHTML = contributorData[index].score
        newRow.appendChild(noOfCommits);

        newRow.onclick = clickHandler(table.contributorList[index].author.login);

        // append row to table
        tableBody.appendChild(newRow);

    }

    return tableBody;
}

function listOfContributors(fileStats){

    let table = document.getElementById("file_table");
    table.style.display = "table";
    //var fileStats = localStorage.getItem("fileStats")
    table.appendChild(buildTableHead());
    table.appendChild(buildTableBody(fileStats));

}
