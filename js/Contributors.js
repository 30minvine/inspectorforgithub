"use strict"

// check whether the owner and the repo name is stored in localStorage
if (localStorage.getItem("owner") === null || localStorage.getItem("repo") === null){
  window.location.href = "MainPage.html";
}


const fetchContributors = async (owner,repo) => {
    /*
    Fetches a array of contributors using GitHub API
    GitHub documentation: https://developer.github.com/v3/repos/statistics/#get-contributors-list-with-additions-deletions-and-commit-counts

    return: data - fetched contributor array
    */
    const api_call = await fetch(`https://api.github.com/repos/${owner}/${repo}/stats/contributors`);
    if (api_call.status == 204){
        // no content/contributors
        return 204;
    }
    const data = await api_call.json();
    return data
    }


class ContributorTable  {
    // used to store info about table contents and pages
    constructor(tableNode, usersPerPage){
        this.pageVal = 1; //start with first page
        this.tableNode = tableNode; // <table> node, from which the data will be attached
        this.contributorList = []; // list of displayed contributors. Changes based on search.
        this.totalList = []; // list of all contributors
        this.usersPerPage = usersPerPage;
        this.loaded = false; // is table displayed?
    }

    set page(newPage){
        // check is new page is valid
        if (Number.isInteger(newPage) || newPage >= 1 || newPage <= Math.ceil(this.contributorList.length/this.usersPerPage) /*last possible page*/){
            this.pageVal = newPage;
            console.log("Page was changed to "+toString(newPage));
            console.log(this.loaded);
        }
        else{
            console.log("Tried changing page to "+toString(newPage));
        }

        // update bar and table if needed
        if (this.loaded){
            swapNode(buildTableBody(this), document.getElementById("contributorTableBody"));

            // create new bar, delete old bar
            swapNode(buildPaginationBar(this), document.getElementById("paginationRow"));
        }
    }

    get page(){
        return this.pageVal;
    }

    get pageStart(){
        return (this.contributorList.length-1) - this.usersPerPage*(this.page-1);
    }

    get pageEnd(){
        let pageEnd = this.pageStart - this.usersPerPage +1;
        if (pageEnd < 0){
            pageEnd = 0;
        }
        return pageEnd;
    }

    get lastPage(){
        return Math.ceil(this.contributorList.length/this.usersPerPage);
    }
}


function swapNode(newNode, oldNode){
    /*
    'Swaps' oldNode with newNode. Not a true swap; if the parent node has other children,
    newNode wont be in the same position as oldNode.
    */
    let parent = oldNode.parentNode;
    parent.removeChild(oldNode);
    parent.appendChild(newNode);
}


function createSearching(table){
    let input = document.getElementById("filter");
    input.tableParam = table;
    
    input.addEventListener("keyup", (inputArg) => {
        let table = inputArg.target.tableParam;
        let input = document.getElementById("filter");
        let filter = input.value.toUpperCase();
        
        table.contributorList = []
        // Loop through all contributors (totalList), and add those that the search query
        for (let i = 0; i < table.totalList.length; i++) {
            let contributor = table.totalList[i].author.login;
            if (contributor.toUpperCase().indexOf(filter) > -1) {
                // has filter term, so display it
                table.contributorList.push(table.totalList[i]);
            }
        }
        // set to page 1
        table.page = 1;
    });
}


function displayNoContributors(table){
    /*
    Called when a repository has no contributors. Displays a message saying so.
    */
    let noData = document.createElement("td");
    noData.innerHTML = "This repository has no contributors";
    table.tableNode.appendChild(noData);
    table.loaded = true;
}


function displayError(table){
    let noData = document.createElement("td");
    noData.innerHTML = "Contributor data could not be retrieved";
    table.tableNode.appendChild(noData);
    table.loaded = true;
}


function displayTable(table){
    /*
    Should be called when page is loaded and contributor array has been set. Loads table for the first time.
    */
    if (table.contributorList == 204){
        // if there's no contributors, can't build table
        displayNoContributors(table);
    }
    else {
        table.tableNode.appendChild(buildTableHead());
        table.tableNode.appendChild(buildTableBody(table));
        table.tableNode.appendChild(buildPaginationBar(table));   
    }
    table.loaded = true; // table is displayed
}


function buildTableHead(){
    /*
    Builds Contributor List header node

    return: tableHead - <thead> node containing Contributor List table header ( , Contributors, GitHub ID, Commits)
    */
    let tableHead  = document.createElement("thead");
    let newRow = document.createElement("tr");
    // create column for avatars
    let newHeader = document.createElement("th");
    newHeader.class += "mdl-layout--large-screen-only";
    newRow.appendChild(newHeader);

    newHeader = document.createElement("th");
    newHeader.class += "mdl-data-table__cell--non-numeric";
    newHeader.innerHTML = "Contributor";
    newRow.appendChild(newHeader);

    newHeader = document.createElement("th");
    newHeader.class += "mdl-data-table__cell--non-numeric";
    newHeader.innerHTML = "GitHub ID";
    newRow.appendChild(newHeader);

    newHeader = document.createElement("th");
    newHeader.class += "mdl-data-table__cell--non-numeric";
    newHeader.innerHTML = "Commits";
    newRow.appendChild(newHeader);

    tableHead.appendChild(newRow);
    return tableHead;
}


function buildTableBody(table){
    /*
    Builds Contributor List table page body, using contributorList

    input: currentPage - integer. Page to be built
           usersPerPage - integer, number of contributors to be displayed per page
    return: tableBody - <tbody> node containing table body (avatar, name, id, and total number of commits for all contributors)
    */
    const avatarWidth = 50;
    const avatarHeight = avatarWidth; // to remain square
    let tableBody = document.createElement("tbody");
    tableBody.id = "contributorTableBody";

    // function to be called when row is clicked
    var clickHandler = function(user) {
        return function() {
            localStorage.setItem('profile',user);
            window.location.href = "ContributorProfile.html";
        };
    };

    // going through each contributor and adding them to the table
    for (var index = table.pageStart; index >= table.pageEnd; index--){
        let newRow = document.createElement("tr");

        // add avatar to row
        let avatar = document.createElement("td");
        avatar.className += "mdl-data-table__cell--non-numeric avatar";
        
        let avatarPicture = document.createElement("img");
        avatarPicture.src = table.contributorList[index].author.avatar_url;
        avatarPicture.alt = table.contributorList[index].author.login+"'s avatar";

        avatar.appendChild(avatarPicture);
        newRow.appendChild(avatar);

        // add contributor name to row
        let name = document.createElement("td");
        name.className += "mdl-data-table__cell--non-numeric";
        name.innerHTML = table.contributorList[index].author.login;
        newRow.appendChild(name);

        // add contributor id to row
        let id = document.createElement("td");
        id.className += "mdl-data-table__cell--non-numeric"; // set to non-numeric so it'd be in line with the header
        id.innerHTML = table.contributorList[index].author.id;
        newRow.appendChild(id);

        // add total number of commits to row
        let numberOfCommits = document.createElement("td");
        numberOfCommits.className += "mdl-data-table__cell--non-numeric";
        numberOfCommits.innerHTML = table.contributorList[index].total;
        newRow.appendChild(numberOfCommits);

        // making row clickable
        newRow.onclick = clickHandler(table.contributorList[index].author.login);

        // append row to table
        tableBody.appendChild(newRow);
    }
    return tableBody;
}


function buildPaginationButton(table, buttonType){
    /*
    Button for the pagination bar
    
    input:  buttonType - either a page number (int 1,2,3 etc )or an arrow type (string "back" or "forward")
    */

    let button = document.createElement("button");

    // assign class name
    if (buttonType == table.page){
        // makes current page button different colour
        button.className += "mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--accent page-button";
    }
    else{
        button.className += "mdl-button mdl-js-ripple-effect mdl-js-button page-button";
    }

    if (buttonType=="back"){
        button.innerHTML = "|<"
        button.onclick = function() {table.page = 1}
    }   
    else if (buttonType=="forward"){
        button.innerHTML = ">|"
        button.onclick = function() {table.page = table.lastPage}
    }
    else{
        // number button
        button.innerHTML = buttonType.toString();
        if (buttonType != table.page){
            button.onclick = function() {table.page = buttonType};
        }
    }
    return button;  
}


function buildPaginationBar(table){
    /*
    Builds a tool bar to be used for switching between pages

    return: pageBarRow - <tr> node containing the interface for navigating table pages
    */
    let row = document.createElement("tr");
    let data = document.createElement("td");
    data.colSpan = 4;
    data.style = "text-align:center";
    row.id = "paginationRow";

    let sidePages = 1; // number of pages to be displayed left/right of current page
    let minPage = table.page - sidePages; // first numbered page in bar
    let maxPage = table.page + sidePages // last numbered page in bar

    if (table.page > 1){
        data.appendChild(buildPaginationButton(table,"back"));
    }

    for (var i = minPage; i <= maxPage; i++ ){
        if (i >= 1 & i <= table.lastPage){
            data.appendChild(buildPaginationButton(table, i));
        }
    }

    if (table.page < table.lastPage){
        data.appendChild(buildPaginationButton(table, "forward"));
    }

    row.appendChild(data)
    return row;
}


function onPageLoad(){
    /* 
    To be called when contributors.html is loaded
    */
    
    document.getElementById("ownerAndRepo").innerHTML = `${localStorage.getItem('owner')} / ${localStorage.getItem('repo')}`
    
    var table = new ContributorTable(document.getElementById("contributor_table"), 15);
    table.tableNode.style.display = "table";
    let spinner = document.getElementById("spinner");

    fetchContributors(localStorage.getItem("owner"), localStorage.getItem("repo")).then(function(fetchedList){
        // delete loading spinner
        spinner.parentNode.removeChild(spinner);
        table.contributorList = fetchedList;
        table.totalList = fetchedList;
        
        createSearching(table);
        displayTable(table);


    }, function(contributorsList){
        // list was not fetched, display error
        spinner.parentNode.removeChild(spinner);
        displayError(table);
    });     
}