//"use strict"
let getInfoRef = document.getElementById("getInfo");
let optionsRef = document.getElementById("options");
createElement("displayURLFormat");

// Retrieving Input URL when in displayURLFormat mode
let inputRef;

// Retreive Owner and Repo URL when in displayOwnerRepo mode
let ownerRef;
let repoRef;

// Reference to errorlabels so that message can be changed
let errorRef = document.getElementsByClassName("mdl-textfield__error")

const search = (e) => {
  if (e.keyCode===13) {
    getUrl()
  }
}
// Returns True if repository exists, false if not
const fetchRepositoryStatus = async (owner,repo) => {
  const api_call = await fetch(`https://api.github.com/repos/${owner}/${repo}`)
  return api_call.ok
}

// Returns True if repository is not empty
const fetchRepositoryContents = async (owner,repo) => {
  const api_call = await fetch(`https://api.github.com/repos/${owner}/${repo}/git/trees/master?recursive=1`);
  return api_call.ok
}

const getUrl = async (mode) => {
    try {
      let owner,repo;
      if (mode==="displayURLFormat")
      {
        var values = inputRef.value;
        // splitting the url obtained from the user by '/' and inputting it into an array
        values = values.split("/");
        // further split the last value to only take the "repo" name if there is ".git"
        values[4] = values[4].split(".git");

        owner = values[3]
        repo = values[4][0]
      } else if (mode==="displayOwnerRepoFormat")
      {
        owner = document.getElementById("inputOwner").value
        repo = document.getElementById("inputRepo").value
      }
      const EXISTS = await fetchRepositoryStatus(owner,repo)
      // Throw error is repository does not exist or is private
      if (!EXISTS) {
        throw new Error("Repository Does Not Exist Or Is Private")
      }
      // Throw error if repository is empty
      const EMPTY = await fetchRepositoryContents(owner,repo)
      if (!EMPTY) {
        throw new Error("Repository Is Empty")
      }
      // taking the last and second last value and stored it on localStorage as "owner" and "repo"
      localStorage.setItem("owner", owner);
      localStorage.setItem("repo", repo);
      location.href = "Overview.html"
    }  catch (err) {
      // If error is because from the split function throwing an error then set error message to invalid error
      if (err.name==="TypeError") {
        err.message = "Invalid URL"
      }
      activeError(err.message,mode)
    }

}

function activeError(message,mode) {

  errorRef[0].innerText = message

  if (mode==="displayURLFormat")
  {

    inputRef.parentNode.classList.add("is-invalid");
    inputRef.parentNode.classList.add("is-dirty");
  } else if (mode==="displayOwnerRepoFormat")
  {
    ownerRef.parentNode.classList.add("is-invalid");
    ownerRef.parentNode.classList.add("is-dirty");
    repoRef.parentNode.classList.add("is-invalid");
    repoRef.parentNode.classList.add("is-dirty");
  }

}

function chosenImage(url_string) {
     document.body.style.backgroundImage = `url(${url_string})`
}

function createElement(format){

  // create a new Row
  var newRow = document.createElement('tr');
  newRow.className += "mdl-data-table__cell--non-numeric";

  // create a new column for inputText
  var inputTextColumn = document.createElement('td');
  inputTextColumn.className += "mdl-data-table__cell--non-numeric";
  inputTextColumn.id = "test"
  inputTextColumn.style.textAlign += "flex-end";

  // this will have an if-else statement
  if (format === "displayOwnerRepoFormat"){
    inputTextColumn.appendChild(displayOwnerRepoFormat());
  }
  else if (format === "displayURLFormat"){
    inputTextColumn.appendChild(displayURLFormat());
  }


  // create a new column for search Button
  var searchButtonColumn = document.createElement('td');
  searchButtonColumn.id = "searchButton";
  searchButtonColumn.className += "mdl-data-table__cell--non-numeric";
  searchButtonColumn.style.alignItems += "flex-end";



  // create Search button
  var searchButton = document.createElement("a");
  searchButton.className += "mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--colored";
  searchButton.innerHTML += "<i class=\"material-icons\">search</i>";
  searchButton.onclick = function(){
                            if (format === "displayOwnerRepoFormat"){
                              ownerRef = document.getElementById("inputOwner")
                              repoRef = document.getElementById("inputRepo")
                              getUrl("displayOwnerRepoFormat")

                            }
                            else if (format === "displayURLFormat"){
                              inputRef = document.getElementById("inputUrl")
                              getUrl("displayURLFormat")
                            }
                        }
  searchButtonColumn.appendChild(searchButton);




  // append columns into row1
  newRow.appendChild(inputTextColumn);
  newRow.appendChild(searchButtonColumn);


  getInfoRef.appendChild(newRow);
  optionsRef.appendChild(options());
}

function options(){

    var row = document.createElement("tr");
    let buttons = document.createElement("td");
    buttons.colSpan = 2;
    buttons.style = "text-align:center";

    var buttonURL = document.createElement("button");
    buttonURL.className = "mdl-button mdl-js-button mdl-button--primary";
    buttonURL.innerHTML = "URL";
    //buttonURL.onclick = function(){removeElements("displayURLFormat")}

    buttonURL.addEventListener("click", function(e){
      // Strange bug where pressing enter on textfields triggers this event listener.
      // If statement rejects these curious events
        if (e.x!==0||e.y!==0) {
          removeElements()
          createElement("displayURLFormat");
          componentHandler.upgradeDom();
        }
    })

    //buttonURL.addEventListener("click", removeElements("displayURLFormat"));
    buttons.appendChild(buttonURL);

    var buttonOwnerRepo = document.createElement("button");
    buttonOwnerRepo.className = "mdl-button mdl-js-button mdl-button--primary";
    buttonOwnerRepo.innerHTML = "Owner & Repo";
    //buttonOwnerRepo.onclick = function(){removeElements("displayOwnerRepoFormat")}

    buttonOwnerRepo.onclick = function(){
              removeElements()
              createElement("displayOwnerRepoFormat")
              componentHandler.upgradeDom();
    }
    //buttonOwnerRepo.addEventListener("click", removeElements("displayOwnerRepoFormat"))
    buttons.appendChild(buttonOwnerRepo);

    row.appendChild(buttons);
    return row;
}

function displayOwnerRepoFormat(){

    var newColumn = document.createElement('td');
    newColumn.id = "newColumn"
    newColumn.className += "mdl-data-table__cell--non-numeric";

    // create a column for the owner
    var inputTextOwner = document.createElement("div");
    inputTextOwner.className = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label";
    inputTextOwner.id = "inputTextOwner";

    var ownerColumn = document.createElement('tr');
    ownerColumn.className += "mdl-data-table__cell--non-numeric";
    inputTextOwner.innerHTML = "<input class=\"mdl-textfield__input\" id=\"inputOwner\" ></input>";
    inputTextOwner.innerHTML += "<label class=\"mdl-textfield__label\" for=\"inputOwner\">Owner</label>";
    inputTextOwner.innerHTML += "<span class=\"mdl-textfield__error\" id=\"errorLabel\"></span>"
    ownerColumn.appendChild(inputTextOwner);


    // create a column for the repo
    var inputTextRepo = document.createElement("div");
    inputTextRepo.className = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label";
    inputTextRepo.id = "inputText";

    var repoColumn = document.createElement('tr');
    repoColumn.className += "mdl-data-table__cell--non-numeric";
    inputTextRepo.innerHTML = "<input class=\"mdl-textfield__input\" id=\"inputRepo\" ></input>";
    inputTextRepo.innerHTML += "<label class=\"mdl-textfield__label\" for=\"inputRepo\">Repository</label>";
    inputTextRepo.innerHTML += "<span class=\"mdl-textfield__error\" id=\"errorLabel\"></span>"

    inputTextOwner.getElementsByTagName("input")[0].addEventListener("keypress",(e)=>{
      if (e.keyCode===13) {
        document.getElementById("inputRepo").focus()
    }})
    inputTextRepo.getElementsByTagName("input")[0].addEventListener("keypress",(e)=>{
      if (e.keyCode===13) {
      ownerRef = document.getElementById("inputOwner")
      repoRef = document.getElementById("inputRepo")
      getUrl("displayOwnerRepoFormat")
      }
    })

    ownerColumn.appendChild(inputTextRepo);

    newColumn.appendChild(ownerColumn);
    newColumn.appendChild(repoColumn);

    return newColumn;

}

function displayURLFormat(){
    /*
    Displaying textfield in the URL format
    */

    // creating textField class
    var inputTextURL = document.createElement("div");
    inputTextURL.className = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label";
    inputTextURL.id = "inputTextURL";
    inputTextURL.innerHTML = "<input class=\"mdl-textfield__input\" id=\"inputUrl\"></input>";
    inputTextURL.innerHTML += "<label class=\"mdl-textfield__label\" for=\"inputUrl\">Enter Repository URL</label>"
    inputTextURL.innerHTML += "<span class=\"mdl-textfield__error\" id=\"errorLabel\"></span>"

    inputTextURL.getElementsByTagName("input")[0].addEventListener("keypress",(e)=>{
      if (e.keyCode===13) {
        inputRef = document.getElementById("inputUrl")
        getUrl("displayURLFormat")
      }
    })
    return inputTextURL;

}

function removeElements(){
  /*
  reset the whole page
  */

  // getting the parentnode through ID
  let getInfoRef = document.getElementById("getInfo");
  let optionsRef = document.getElementById("options");

  // removing childNodes in each parentnode
  while(optionsRef.firstChild){
    optionsRef.removeChild(optionsRef.firstChild);
  }

  while (getInfoRef.firstChild){
    getInfoRef.removeChild(getInfoRef.firstChild);
  }

}
